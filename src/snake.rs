use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    core::transform::Transform,
    ecs::{Component, DenseVecStorage},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};

pub const ARENA_HEIGHT: f32 = 320.0;
pub const ARENA_WIDTH: f32 = 240.0;

pub const SEGMENT_SIZE: f32 = 16.0;

#[derive(PartialEq, Eq)]
pub enum SegmentType {
    Head,
    Body,
    Tail,
}

pub struct Segment {
    pub segment: SegmentType,
    pub width: f32,
    pub height: f32,
}

impl Segment {
    fn new(segment: SegmentType) -> Segment {
        Segment {
            segment,
            width: SEGMENT_SIZE,
            height: SEGMENT_SIZE,
        }
    }
}

impl Component for Segment {
    type Storage = DenseVecStorage<Self>;
}

pub struct Snake;

impl SimpleState for Snake {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world);

        world.register::<Segment>();

        init_segments(world, sprite_sheet_handle);
        init_camera(world);
    }
}

fn load_sprite_sheet (world: &mut World) -> Handle<SpriteSheet> {
    // Load the sprite sheet necessary to render the graphics
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/spritesheet_snake.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/spritesheet_snake.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

fn init_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    transform.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(ARENA_WIDTH, ARENA_HEIGHT))
        .with(transform)
        .build();
}

// Initialise starting segments of the snake
fn init_segments(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let mut head_transform = Transform::default();
    let mut body_transform = Transform::default();
    let mut tail_transform = Transform::default();

    // Assign the sprites for the segments
    let sprite_render_head = SpriteRender::new(sprite_sheet_handle.clone(), 3);
    let sprite_render_body = SpriteRender::new(sprite_sheet_handle.clone(), 8);
    let sprite_render_tail = SpriteRender::new(sprite_sheet_handle.clone(), 10);

    // Position head in the middle
    let y = ARENA_HEIGHT / 2.0;
    head_transform.set_translation_xyz(ARENA_WIDTH * 0.5, y, 0.0);
    body_transform.set_translation_xyz(ARENA_WIDTH * 0.5 + SEGMENT_SIZE, y, 0.0);
    tail_transform.set_translation_xyz(ARENA_WIDTH * 0.5 + SEGMENT_SIZE * 2.0, y, 0.0);

    // Create head
    world
        .create_entity()
        .with(sprite_render_head.clone())
        .with(head_transform)
        .with(Segment::new(SegmentType::Head))
        .build();

    // Create body
    world
        .create_entity()
        .with(sprite_render_body.clone())
        .with(body_transform)
        .with(Segment::new(SegmentType::Body))
        .build();

    // Create tail
    world
        .create_entity()
        .with(sprite_render_tail.clone())
        .with(tail_transform)
        .with(Segment::new(SegmentType::Tail))
        .build();
}
